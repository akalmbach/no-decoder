import numpy as np
import matplotlib.pyplot as plt
import librosa
import torch
import torch.optim as optim

from no_decoder.dataset import SongDataset
from no_decoder.nn_utils import LSTM
from no_decoder.resnet import ResNet
from no_decoder.chroma_utils import RMSLoss, VocoderLoss


source_file = '/media/arnold/kola/Music/Library/Beach_Fossils/Beach_Fossils/01_-_Sometimes.mp3'
HOP_SIZE = 4096
EPOCHS = 1000
if __name__ == '__main__':

    sds = SongDataset(source_file)

    net = ResNet()
    print(net)
    vloss = VocoderLoss(HOP_SIZE, 48000)
    basic_loss = RMSLoss(HOP_SIZE // 2)
    optimizer = optim.Adam(net.parameters(), lr=1e-5)

    batch_size = 8
    sdl = torch.utils.data.DataLoader(sds, batch_size=batch_size, shuffle=True)
    sdl_val = torch.utils.data.DataLoader(sds, batch_size=1, shuffle=False)

    for epoch in range(EPOCHS):
        vloss.qcut = min(10, epoch + 1)
        for i, (targets, inputs) in enumerate(sdl):
            optimizer.zero_grad()

            outputs = net(targets)
            if torch.isnan(outputs).any():
                import ipdb; ipdb.set_trace()

            print(outputs.min(axis=1)[0].detach(), outputs.max(axis=1)[0].detach())
            print(targets.min(axis=1)[0], targets.max(axis=1)[0])
            vocoder_loss = vloss(outputs, targets)
            rms_loss = basic_loss(outputs, targets)
            if torch.isnan(vocoder_loss).any():
                import ipdb; ipdb.set_trace()
            vocoder_loss = vocoder_loss.sum()

            (vocoder_loss + rms_loss).backward()
            optimizer.step()
            print(
                f"E{epoch}.{i}: Loss {vocoder_loss.item():.3f}, {rms_loss.item():.4f}"
            )

        if epoch % 1 == 0:
            net.train(False)
            outbuf = []
            for i, (targets, inputs) in enumerate(sdl_val):
                if i % 8 == 0:
                    print(i / len(sdl_val))
                    optimizer.zero_grad()
                    with torch.no_grad():
                        outputs = net(targets)
                        outputs = outputs.squeeze().numpy()
                        outbuf.append(outputs)

            librosa.output.write_wav(f'./epoch_{epoch}.wav', np.concatenate(outbuf), 48000)
