# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import os
from pip._internal.req import parse_requirements

requirements = parse_requirements('requirements.txt', session=False)

scripts = []
for filename in os.listdir("scripts"):
    if filename.endswith(".py"):
        scripts.append(filename)

here = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='no_decoder',
    version='0.0.0',
    description='Experimental lightweight neural synthesizers and waveshapers.', 
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/akalmbach/no-decoder',
    author='Arnold Kalmbach',
    author_email='akalmbach@gmail.com',
    packages=find_packages(),
    scripts=[
        os.path.join('scripts', script) for script in scripts
    ],
    python_requires='>=3.6',
    include_package_data=True,
    install_requires=[
        str(req.req) for req in requirements
    ]
)
