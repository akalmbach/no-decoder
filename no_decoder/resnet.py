import torch.nn as nn


class ResLayer(nn.Module):
    def __init__(self, in_channels, out_channels, ksize=3, nonlin=lambda: lambda x: x, p_drop=0.0):
        super().__init__()
        if in_channels != out_channels:
            self.downsample = nn.Sequential(
                nn.Conv1d(
                    in_channels, out_channels,
                    kernel_size=1, padding=0,
                ),
                nn.BatchNorm1d(out_channels)
            )
        else:
            self.downsample = None

        padding = ksize // 2
        self.block = nn.Sequential(
            nn.Conv1d(
                in_channels, out_channels,
                kernel_size=ksize, padding=padding,
            ),
            nn.BatchNorm1d(out_channels),
            nonlin(),
            nn.Dropout(p=p_drop),
            nn.Conv1d(
                out_channels, out_channels,
                kernel_size=ksize, padding=padding,
            ),
            nn.BatchNorm1d(out_channels),
        )
        self.nonlin = nonlin()

    def forward(self, x):
        if self.downsample is None:
            shortcut = x
        else:
            shortcut = self.downsample(x)

        outs = self.block(x)
        outs = self.nonlin(shortcut + outs)

        return outs


class ResBlock(nn.Module):
    def __init__(self, in_channels, out_channels, num_layers, downsample_size=2):
        super().__init__()
        layers = [ResLayer(in_channels, out_channels, nonlin=nn.LeakyReLU)]
        for i in range(num_layers):
            layers.append(ResLayer(out_channels, out_channels, nonlin=nn.LeakyReLU))
        self.layers = nn.Sequential(*layers)
        if downsample_size is None:
            self.downsample = lambda x: x
        else:
            self.downsample = nn.AvgPool1d(
                kernel_size=downsample_size,
                stride=downsample_size,
            )

    def forward(self, x):
        x = self.layers(x)
        x = self.downsample(x)
        return x


class ResNet(nn.Module):
    def __init__(self, block_sizes=[(1, 1), (4, 16)]):
        super().__init__()
        layers = []
        for i in range(1, len(block_sizes)):
            _, prev_size = block_sizes[i - 1]
            num_layers, cur_size = block_sizes[i]
            block = ResBlock(prev_size, cur_size, num_layers, downsample_size=None)
            layers.append(block)
        layers.append(nn.Conv1d(block_sizes[-1][1], 1, kernel_size=1))
        layers.append(nn.Tanh())
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        x = x.view((x.shape[0], 1, x.shape[1]))
        x = self.layers(x)
        x = x[:, 0, :]
        return x
