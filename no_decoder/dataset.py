import torch
import librosa


class SongDataset(torch.utils.data.Dataset):
    def __init__(self, source_file, hop_size=4096, sr=48000, hops_per_sample=8):
        super().__init__()
        self.hop_size = hop_size
        self.raw_data, sr = librosa.load(source_file, sr)
        total_hops = len(self.raw_data) // hop_size
        self.raw_data = self.raw_data[:total_hops * hop_size]
        self.raw_data = torch.Tensor(self.raw_data).reshape((total_hops, hop_size))
        self.noise = torch.randn_like(self.raw_data)
        self.hops_per_sample = 8

    def __len__(self):
        return self.raw_data.shape[0] - self.hops_per_sample

    def __getitem__(self, i):
        outs = self.raw_data[i:i + self.hops_per_sample].flatten()
        noise = self.noise[i:i + self.hops_per_sample].flatten()
        return outs, noise
