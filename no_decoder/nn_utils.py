import torch
import torch.nn as nn


class MLPBlock(nn.Module):
    def __init__(self, in_channels, out_channels, bn=False, nonlinearity=None, **conv_kwargs):
        super().__init__()
        self.fc = nn.Linear(in_channels, out_channels, **conv_kwargs)
        if nonlinearity is None:
            self.nonlinearity = lambda x: x
        else:
            self.nonlinearity = nonlinearity()

        if bn:
            self.bn = nn.BatchNorm1d(in_channels)
        else:
            self.bn = lambda x: x

    def forward(self, x):
        x = self.bn(x)
        x = self.fc(x)
        x = self.nonlinearity(x)
        return x


class ConvBlock(nn.Module):
    # TODO: Replace with ResBlock or another better cell
    def __init__(self, in_channels, out_channels, bn=False, nonlinearity=None, **conv_kwargs):
        super().__init__()
        self.conv = nn.Conv1d(in_channels, out_channels, **conv_kwargs)
        if nonlinearity is None:
            self.nonlinearity = lambda x: x
        else:
            self.nonlinearity = nonlinearity()

        if bn:
            self.bn = nn.BatchNorm1d(in_channels)
        else:
            self.bn = lambda x: x

    def forward(self, x):
        x = self.bn(x)
        x = self.conv(x)
        x = self.nonlinearity(x)

        return x


class LSTMBlock(nn.Module):
    def __init__(self, st_size=2048, lt_size=256):
        super().__init__()
        self.lt_size = lt_size
        self.st_size = st_size
        self.input_gate = nn.Sequential(
            ConvBlock(2, lt_size * 2, kernel_size=3, padding=1),
            nn.AvgPool1d(st_size)
        )
        self.forget_gate = nn.Sequential(
            ConvBlock(2, lt_size, kernel_size=3, padding=1),
            nn.AvgPool1d(st_size)
        )
        self.st_output_gate = nn.Sequential(
            ConvBlock(2, lt_size, kernel_size=3, padding=1, nonlinearity=nn.LeakyReLU),
            ConvBlock(lt_size, 2, kernel_size=3, padding=1),
        )
        self.lt_output_gate = nn.Sequential(
            MLPBlock(lt_size, 2 * st_size),
        )
        self.final_conv = nn.Sequential(
            ConvBlock(1, 12, kernel_size=3, padding=1, nonlinearity=nn.LeakyReLU),
            # ConvBlock(12, 96, kernel_size=3, padding=1, nonlinearity=nn.LeakyReLU),
            # ConvBlock(96, 12, kernel_size=3, padding=1, nonlinearity=nn.LeakyReLU),
            ConvBlock(12, 1, kernel_size=3, padding=1, nonlinearity=nn.Tanh)
        )

    def forward(self, st, lt):
        inputs = self.input_gate(st)
        update_vector = inputs[:, :self.lt_size, 0].sigmoid()
        update_vector = update_vector * inputs[:, self.lt_size:, 0].tanh()
        forget_vector = self.forget_gate(st)[:, :, 0].sigmoid()

        lt = lt * forget_vector
        lt = lt + update_vector

        st1 = self.st_output_gate(st)
        st2 = self.lt_output_gate(lt).reshape((-1, 2, self.st_size))
        st = st1.sigmoid() * st2.tanh()
        outs = self.final_conv(st[:, [1]])[:, 0]
        st = st[:, 0]

        return outs, st, lt


class LSTM(nn.Module):
    def __init__(self, st_size=2048, lt_size=256):
        super().__init__()
        self.st_size = st_size
        self.lt_size = lt_size
        self.cell = LSTMBlock(st_size, lt_size)

    def forward(self, x, st_init, lt_init):
        x = x.reshape((x.shape[0], -1, self.st_size))
        x = x.permute([1, 0, 2])
        x = x[..., None]

        st = st_init
        lt = lt_init
        outs = []
        for i, xchunk in enumerate(x):
            st = st[..., None]
            st_in = torch.cat((xchunk, st), axis=-1)

            st_in = st_in.permute([0, 2, 1])
            out, st, lt = self.cell(st_in, lt)
            outs.append(out)

        return torch.cat(outs, axis=1)
