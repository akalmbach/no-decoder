import numpy as np
import torch
import torch.nn as nn
import torchaudio
from librosa import filters
import matplotlib.pyplot as plt


def axis_flatten(x, axis=0, result_ndims=1):
    ndims = len(x.shape)
    end_dim = axis % ndims
    roll = end_dim - ndims + result_ndims
    ax_index = [(i + roll) % ndims for i in range(ndims)]
    x = x.permute(ax_index)

    pre_gather_shape = x.shape[:-result_ndims]
    final_dim_size = x.shape[-result_ndims:]
    x = x.reshape((-1,) + final_dim_size)
    return x, pre_gather_shape


def axis_unflatten(y, pre_gather_shape, axis=0, result_ndims=1):
    ndims = len(pre_gather_shape) + result_ndims
    end_dim = axis % ndims
    roll = end_dim - len(pre_gather_shape)

    y = y.reshape(pre_gather_shape + y.shape[-result_ndims:])
    ax_index = [(i - roll) % len(y.shape) for i in range(len(y.shape))]
    y = y.permute(ax_index)
    return y


def axis_stft(x, axis=1, **stft_kwargs):
    x, inshape = axis_flatten(x, axis)
    y = torch.stft(x, **stft_kwargs)
    y = axis_unflatten(y, inshape, axis, result_ndims=3)
    return y


def axis_istft(y, axis=1, **stft_kwargs):
    y, inshape = axis_flatten(y, axis, result_ndims=3)
    x = torchaudio.functional.istft(y, **stft_kwargs)
    x = axis_unflatten(x, inshape, axis, result_ndims=1)
    return x


class ChromaLayer(nn.Module):
    def __init__(self, sr, n_fft, n_chroma=12, norm=2, axis=1):
        super().__init__()
        self.axis = axis
        chroma_fb = filters.chroma(sr, n_fft, A440=440.0, n_chroma=n_chroma)
        self.n_freq_channels = n_fft // 2 + 1
        self.n_chroma = n_chroma
        self.norm = norm
        self.linear = nn.Linear(self.n_freq_channels, n_chroma, bias=False)
        self.linear.weight.data = torch.Tensor(chroma_fb)

    def forward(self, spec):
        spec_flat, in_shape = axis_flatten(spec, axis=self.axis)
        chroma_flat = self.linear(spec_flat)
        chroma = axis_unflatten(chroma_flat, in_shape, axis=self.axis)
        norm = (chroma**self.norm).sum(axis=1)**(1 / self.norm)
        chroma = chroma / norm[:, None, :]
        return chroma


class SpectralCentroidLayer(nn.Module):
    def __init__(self, sr, n_fft, axis=1):
        super().__init__()
        self.fft_freqs = nn.Parameter(torch.Tensor(
            np.linspace(0, float(sr) / 2, int(1 + n_fft // 2), endpoint=True)
        )[:, None])
        self.axis = axis

    def forward(self, spec):
        '''Is this a reasonable algorithm?'''
        norm = spec.sum(axis=1)
        spec = spec / norm[:, None, :]
        spec_flat, in_shape = axis_flatten(spec, axis=self.axis)
        centroid = spec_flat.mm(self.fft_freqs)
        centroid = axis_unflatten(centroid, in_shape, axis=self.axis, result_ndims=1)
        return centroid


class VocoderLoss(nn.Module):
    def __init__(self, n_fft, sr, qcut=8):
        super().__init__()
        self.n_fft = n_fft
        self.sr = sr
        self.window = nn.Parameter(torch.hann_window(self.n_fft))
        self.qcut = qcut
        self.weights = nn.Parameter(torch.logspace(1, 0, self.n_fft // 2 + 1))
        self.weights.data = self.weights.data / self.weights.data.sum()
        print(self.weights.data)

    def get_log_magnitude_spectrum(self, x):
        stft = axis_stft(x, axis=1, n_fft=self.n_fft, window=self.window)
        stft = (1 + (stft[..., 0]**2 + stft[..., 1]**2).sqrt()).log()
        return stft

    def get_spectral_envelope(self, x):
        mag_spec = self.get_log_magnitude_spectrum(x)
        mag_spec_flat, pre_gather_shape = axis_flatten(mag_spec, axis=1)
        ceps_flat = torch.rfft(mag_spec_flat, signal_ndim=1)
        ceps_flat[:, 1:self.qcut] = ceps_flat[:, 1:self.qcut] * 2
        ceps_flat[:, self.qcut:-1] = 0
        lifter_spec_flat = torch.irfft(ceps_flat, signal_ndim=1)
        # TODO: Unflatten back to input shape
        return lifter_spec_flat, mag_spec

    def forward(self, x_pred, x_true):
        # env_pred, spec_pred = self.get_spectral_envelope(x_pred)
        # env_true, spec_true = self.get_spectral_envelope(x_true)
        # return ((env_pred - env_true)**2).mean(axis=-1)
        env_true, spec_true = self.get_spectral_envelope(x_true)
        env_pred, spec_pred = self.get_spectral_envelope(x_pred)

        diff = ((env_true - env_pred)**2)
        diff = diff.reshape((spec_pred.shape))
        diff = torch.matmul(diff.permute([0, 2, 1]), self.weights)
        return diff.mean(axis=-1).sum()


class RMSLoss(nn.Module):
    def __init__(self, winsize):
        super().__init__()
        self.rms_pool = nn.LPPool1d(2, kernel_size=winsize, stride=winsize // 2)

    def forward(self, x_pred, x_true):
        dc_loss = (x_pred.mean(axis=1) - x_true.mean(axis=1))**2
        rms_pred = self.rms_pool(x_pred[:, None])[:, 0]
        rms_true = self.rms_pool(x_true[:, None])[:, 0]
        rms_loss = ((rms_pred - rms_true)**2).mean(axis=1).sqrt()

        return (dc_loss + rms_loss).sum()


class SpectralLoss(nn.Module):
    def __init__(self, n_fft, sr):
        super().__init__()
        self.n_fft = n_fft
        self.chroma_layer = ChromaLayer(sr, n_fft, axis=1, norm=2)
        self.centroid_layer = SpectralCentroidLayer(sr, n_fft, axis=1)
        self.window = nn.Parameter(torch.hann_window(self.n_fft))

    def get_spec(self, x):
        stft = axis_stft(x, axis=1, n_fft=self.n_fft, window=self.window)
        return stft[..., 0]**2 + stft[..., 1]**2

    def forward(self, x_pred, x_true):
        spec_pred = self.get_spec(x_pred)
        chroma_pred = self.chroma_layer(spec_pred)
        centroid_pred = self.centroid_layer(spec_pred)

        spec_true = self.get_spec(x_true)
        chroma_true = self.chroma_layer(spec_true)
        centroid_true = self.centroid_layer(spec_true)

        if torch.isnan(spec_pred).any() or torch.isnan(spec_true).any():
            print("Got nan spec")

        if torch.isnan(chroma_pred).any() or torch.isnan(centroid_pred).any():
            print("Got nan pred")

        if torch.isnan(chroma_true).any() or torch.isnan(centroid_true).any():
            print("Got nan true")

        chroma_loss = ((chroma_pred - chroma_true)**2).sum(axis=1)
        chroma_loss[torch.isnan(chroma_true).any(axis=1)] = 0
        centroid_loss = ((centroid_pred - centroid_true)**2).sum(axis=1) / self.n_fft
        centroid_loss[torch.isnan(centroid_true).any(axis=1)] = 0

        return chroma_loss, centroid_loss
